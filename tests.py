from django.test import TestCase


class AnimalTestCase(TestCase):
    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        a = 5
        b = 3
        self.assertEqual(b, 2)
        self.assertEqual(a, 5)
