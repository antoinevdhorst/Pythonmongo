# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-17 11:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('demo', '0003_sqlcar_sqlcookie'),
    ]

    operations = [
        migrations.AddField(
            model_name='sqlcookie',
            name='consumer',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='demo.SqlUser'),
            preserve_default=False,
        ),
    ]
