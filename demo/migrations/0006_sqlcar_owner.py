# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-17 12:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('demo', '0005_auto_20170317_1111'),
    ]

    operations = [
        migrations.AddField(
            model_name='sqlcar',
            name='owner',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='demo.SqlUser'),
            preserve_default=False,
        ),
    ]
