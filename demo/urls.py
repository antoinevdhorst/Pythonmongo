from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'addDocument/$', views.add_document, name='addDocument'),
    url(r'results/$', views.results, name='results'),
    url(r'removeDocument/$', views.remove_document, name='removeDocument'),
    url(r'changedocument/$', views.change_document, name='changedocument'),
    url(r'bulkinsert/$', views.bulkinsert, name='bulkinsert'),
    url(r'addonetoomanydocument/$', views.add_one_to_many_document, name='addonetoomanydocument'),
]
